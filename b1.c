#include "b.h"

/*
 * Code generation (x86 assembly)
 */

void
push(void)
{
	//printf("\tpush\t%%eax\t\" XXX push()\n");
	trace ("push()\n");
	printf ("\tadlx7\t=1\n");
	printf ("\tsta\t0,7\n");
}

void
pop(char *s)
{
	//printf("\tpop\t%%%s\t\" XXX pop()\n", s);
	trace ("pop(%s)\n", s);
	printf ("\t%s\t0,7\n", s);
	printf ("\tsblx7\t=1\n");
}

void
drop(void)
{
	trace ("drop\n");
	printf ("\tsblx7\t=1\n");
}

void
binary(struct tnode *tr)
{
	rcexpr(tr->tr1);
	push();
	rcexpr(tr->tr2);
}

int
pushargs(struct tnode *tr)
{
	int stk;

	if (tr == NULL)
		return 0;
	if (tr->op == COMMA) {
		rcexpr(tr->tr2);
		push();
		stk = pushargs(tr->tr1);
		return stk+NCPW;
	}
	rcexpr(tr);
	push();
	return NCPW;
}

void
lvalexp(struct tnode *tr)
{
	struct hshtab *bs;
	char memloc[64];

	switch (tr->op) {

	case DECBEF:
	case INCBEF:
	case DECAFT:
	case INCAFT:
		if (tr->tr1->op == STAR) {
			rcexpr(tr->tr1->tr1);
			printf("\tmov\t%%eax,%%ebx\t\" XXX INCAFT STAR\n");
			sprintf(memloc,"(,%%ebx,4)");
		} else {	/* NAME, checked in "build" */
			bs = (struct hshtab *) tr->tr1->tr1;
			if (bs->class == EXTERN)
				sprintf(memloc,"_%s", bs->name);
			else if (bs->class == AUTO)
				sprintf(memloc,"%d(%%ebp)", bs->offset);
			else
				goto classerror;
		}
		if (tr->op == DECBEF || tr->op == INCBEF) {
			printf("\t%s\t%s\t\" XXX DECBEF/INCBEF\n", tr->op == DECBEF ? "decl" : "incl",
			       memloc);
			printf("\tmov\t%s,%%eax\t\" XXX DECBEF/INCBEF\n", memloc);
		} else {
			printf("\tmov\t%s,%%eax\t\" XXX DECAFT/INCAFT\n", memloc);
			printf("\t%s\t%s\t\" XXX DECAFT/INCAFT\n", tr->op == DECAFT ? "decl" : "incl",
			       memloc);
		}
		return;

	case ASSIGN:
		rcexpr(tr->tr2);
		if (tr->tr1->op == STAR) {
			push();
			rcexpr(tr->tr1->tr1);
			//pop("ebx");
			pop("ldq");
			printf("\tmov\t%%ebx,(,%%eax,4)\t\" XXX ASSIGN STAR\n");
		} else {	/* NAME */
			bs = (struct hshtab *) tr->tr1->tr1;
			if (bs->class == EXTERN)
				printf("\tmov\t%%eax,_%s\t\" XXX ASSIGN EXTERN\n", bs->name);
			else if (bs->class == AUTO)
			{
				//printf("\tmov\t%%eax,%d(%%ebp)\n", bs->offset);
				trace ("ASSIGN AUTO %d\n", bs->offset);
				printf ("\tsta\t%d,7\n", bs->offset);
			}
			else
				goto classerror;
		}
		return;

	case ASPLUS:
	case ASMINUS:
	case ASMOD:
	case ASTIMES:
	case ASDIV:
	case ASOR:
	case ASAND:
	case ASLSH:
	case ASRSH:
	case ASEQUAL:
	case ASNEQL:
	case ASLEQ:
	case ASLESS:
	case ASGTQ:
	case ASGREAT:
		tr->op -= ASPLUS-PLUS;
		rcexpr(block(ASSIGN,0,tr->tr1,tr));
		return;
	}

classerror:
	error("Storage class");
}

void
rcexpr(struct tnode *tr)
{
	int o1, o2;
	int stk;
	struct hshtab *bs;

	if (tr == NULL)
		return;

	if (opdope[tr->op]&02) {
		lvalexp(tr);
		return;
	}

	switch (tr->op) {

	case CON:
		//printf("\tmov\t$%d,%%eax\n", tr->value);
		trace ("rcexpr CON\n");
		printf("\tlda\t=o%o\n", tr->value);
		return;

	case STRING:
		printf("\tmov\t$L%d,%%eax\t\" XXX rcexpr STRING\n", tr->value);
		printf("\tshr\t$2,%%eax\t\" XXX rcexpr STRING\n");
		return;

	case NAME:	/* only rvalue */
		bs = (struct hshtab *) tr->tr1;
		if (bs->class == EXTERN)
		{
			//printf("\tmov\t_%s,%%eax\t\" XXX rcexpr NAME EXTERN\n", bs->name);
			trace ("rcexpr NAME EXTERN\n");
			printf ("\teaa\t<*text>|[%s]\n", bs->name);
		}
		else if (bs->class == AUTO)
		{
			//printf("\tmov\t%d(%%ebp),%%eax\t\" XXX rcexpr NAME AUTO\n", bs->offset);
			trace ("rcexpr NAME AUTO\n");
			printf ("\tlda\t%d,7\n", bs->offset);
		}
		else
			goto classerror;
		return;

	case CALL:
		stk = pushargs(tr->tr2);
		rcexpr(tr->tr1);
		//printf("\tshl\t$2,%%eax\n");
		//printf("\tcall\t*%%eax\n");
		//if (stk)
			//printf("\tadd\t$%d,%%esp\n",stk);
		trace ("CALL %d\n", stk);
// XXX MH-TSS Users Reference, 
		printf ("\ttsx1\t0,al*\n");
// XXX I am quite unsure about this. MH-TSS Users Reference, pg 16:
// "[The first argumnt] is in integer telling how much [the SP] should
// be advance to avoid conflicting with the calling program's use of
// the stack; [the second] contains the number of arguments."
// IIUC, the compiler has pushed the arguments on the stack, so
// the stack pointer should not need adjusting.
		printf ("\tzero\t0,%d\n", stk);
		return;

	case AMPER:
		bs = (struct hshtab *) tr->tr1->tr1;
		if (bs->class == EXTERN) {
			printf("\tmov\t$_%s,%%eax\t\" XXX rcexpr AMPER EXTERN\n", bs->name);
			printf("\tshr\t$2,%%eax\t\" XXX rcexpr AMPER EXTERN\n");
		} else if (bs->class == AUTO) {
			printf("\tlea\t%d(%%ebp),%%eax\t\" XXX rcexpr AMPER AUTO\n", bs->offset);
			printf("\tshr\t$2,%%eax\t\" XXX rcexpr AMPER AUTO\n");
		} else
			goto classerror;
		return;

	case STAR:	/* only rvalue */
		rcexpr(tr->tr1);
		printf("\tmov\t(,%%eax,4),%%eax\t\" XXX rcexpr STAR\n");
		return;

	case PLUS:
		binary(tr);
		//pop("ebx");
		//printf("\tadd\t%%ebx,%%eax\t\" XXX rcexpr PLUS\n");
		trace ("rcexpr PLUS\n");
		printf ("\tada\t0,7\n");
		drop ();
		return;

	case MINUS:
		binary(tr);
		printf("\tmov\t%%eax,%%ebx\t\" XXX rcexpr MINUS\n");
		//pop("eax");
		pop("lda");
		printf("\tsub\t%%ebx,%%eax\t\" XXX rcexpr MINUS\n");
		return;

	case TIMES:
		binary(tr);
		//pop("ebx");
		pop("ldq");
		printf("\tmul\t%%ebx\t\" XXX rcexpr TIMES\n");
		return;

	case DIVIDE:
		binary(tr);
		printf("\tmov\t%%eax,%%ebx\t\" XXX rcexpr DIVIDE\n");
		//pop("eax");
		pop("lda");
		printf("\txor\t%%edx,%%edx\t\" XXX rcexpr DIVIDE\n");
		printf("\tdiv\t%%ebx\t\" XXX rcexpr DIVIDE\n");
		return;

	case MOD:
		binary(tr);
		printf("\tmov\t%%eax,%%ebx\t\" XXX rcexpr MOD\n");
		//pop("eax");
		pop("lda");
		printf("\txor\t%%edx,%%edx\t\" XXX rcexpr MOD\n");
		printf("\tdiv\t%%ebx\t\" XXX rcexpr MOD\n");
		printf("\tmov\t%%edx,%%eax\t\" XXX rcexpr MOD\n");
		return;

	case AND:
		binary(tr);
		//pop("ebx");
		pop("ldq");
		printf("\tand\t%%ebx,%%eax\t\" XXX rcexpr AND\n");
		return;

	case OR:
		binary(tr);
		//pop("ebx");
		pop("ldq");
		printf("\tor\t%%ebx,%%eax\t\" XXX rcexpr OR\n");
		return;

	case LSHIFT:
		binary(tr);
		printf("\tmov\t%%eax,%%ecx\t\" XXX rcexpr LSHIFT\n");
		//pop("eax");
		pop("lda");
		printf("\tshl\t%%cl,%%eax\t\" XXX rcexpr LSHIFT\n");
		return;

	case RSHIFT:
		binary(tr);
		printf("\tmov\t%%eax,%%ecx\t\" XXX rcexpr RSHIFT\n");
		//pop("eax");
		pop("lda");
		printf("\tshr\t%%cl,%%eax\t\" XXX rcexpr RSHIFT\n");
		return;

	case EQUAL:
	case NEQUAL:
	case LESS:
	case LESSEQ:
	case GREAT:
	case GREATEQ:
		binary(tr);
		//pop("ebx");
		pop("ldq");
		printf("\tcmp\t%%eax,%%ebx\t\" XXX rcexpr cmp\n");
		switch (tr->op) {
		case EQUAL:
			printf("\tsete\t%%al\t\" XXX rcexpr cmp EQUAL\n");
			break;
		case NEQUAL:
			printf("\tsetne\t%%al\t\" XXX rcexpr cmp NEQUAL\n");
			break;
		case LESS:
			printf("\tsetl\t%%al\t\" XXX rcexpr cmp LESS\n");
			break;
		case LESSEQ:
			printf("\tsetle\t%%al\t\" XXX rcexpr cmp LESSEQ\n");
			break;
		case GREAT:
			printf("\tsetg\t%%al\t\" XXX rcexpr cmp GREAT\n");
			break;
		case GREATEQ:
			printf("\tsetge\t%%al\t\" XXX rcexpr cmp GREATEQ\n");
			break;
		}
		printf("\tmovzb\t%%al,%%eax\t\" XXX rcexpr cmp\n");
		return;

	case EXCLA:
		rcexpr(tr->tr1);
		printf("\ttest\t%%eax,%%eax\t\" XXX rcexpr EXCLA\n");
		printf("\tsete\t%%al\t\" XXX rcexpr EXCLA\n");
		printf("\tmovzb\t%%al,%%eax\t\" XXX rcexpr EXCLA\n");
		return;

	case NEG:
		rcexpr(tr->tr1);
		printf("\tneg\t%%eax\t\" XXX rcexpr NEG\n");
		return;

	case QUEST:
		cbranch(tr->tr1, o1=isn++, 0);
		rcexpr(tr->tr2->tr1);
		jump(o2 = isn++);
		label(o1);
		rcexpr(tr->tr2->tr2);
		label(o2);
		return;

	default:
		error("Can't print tree (op: %d)", tr->op);
	}

classerror:
	error("Storage class");
}

/* Prints the tree in RPN, for debugging */
/*
void
rcexpr(struct tnode *tr)
{
	struct hshtab *bs;

	if (tr == NULL)
		printf("(NULL) ");
	else if (tr->op == CON)
		printf("%d ", tr->value);
	else if (tr->op == STRING)
		printf("s(L%d) ", tr->value);
	else if (tr->op == NAME) {
		bs = (struct hshtab *)tr->tr1;
		if (bs->class == AUTO)
			printf("%s(%d) ", bs->name, bs->offset);
		else
			printf("%s ", bs->name);
	} else {
		rcexpr(tr->tr1);
		if (opdope[tr->op]&01)
			rcexpr(tr->tr2);
		printtoken(tr->op, stdout);
	}
}
*/
